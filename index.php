<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />

        <title>Curriculum Vitae: Jonathan P Dryhurst Roberts</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Prociono" rel="stylesheet" type="text/css" />
        <link href="includes/resume.css" rel="stylesheet" type="text/css" />

        <script src="https://code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="includes/resume.js" type="text/javascript"></script>
    </head>

    <body>

        <div class="wrapper">

            <div class="resume-body">

                <div id="head">
                    <h1>JONATHAN P DRYHURST ROBERTS</h1>
                    <br />
                    <h2 class="h2-spacer">SENIOR FULL STACK / DEVOPS ENGINEER // TEAM LEAD // ENTREPRENEUR</h2>
                    <br />
                    <h2 class="h2-spacer">20+ YRS. MULTI-NATIONAL EXPERIENCE</h2>
                </div>

                <br />

                <div class="details">
                    <div class="left-33">
                        <span id="address"><a href="http://bit.ly/1lLmhR3">Long Beach, CA 90802</a></span>
                    </div>
                    <div class="left-33">
                        <span id="email"><a href="mailto:jonathan.dryhurst@gmail.com">jonathan.dryhurst@gmail.com</a></span>
                    </div>
                    <div class="left-33">
                        <div id="phone"><a href="tel:562-294-0897">+1 {562) 294-0897</a></div>
                    </div>
                </div>

                <br />

                <br />

                <div id="notes">
                    <h2 class="h2">NOTES</h2>
                    <br />
                    <br />
                    <div class="toggler">
                        <ul>
                            <li>authorized to work in the <span class="country usa">united states</span> for any employer</li>
                            <li>authorized to work in the <span class="country european-union">european union</span> for any employer</li>
                            <li>digital & advertising agency, start-up and corporate experienced</li>
                        </ul>
                    </div>
                </div>

                <div id="work-experience">

                    <h2>WORK EXPERIENCE</h2>

                    <div class="toggler">

                        <br /><br />

                        <div class="employer">
                            <h2>Senior Lead Full Stack / Development Operations Engineer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="title"></li>
                                    <li class="employer-name">Designory</li>
                                    <li class="employer-address">Long Beach, CA</li>
                                    <li class="employer-date">June 2015 to Present</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Server, Network, and Hosting Environment
                                    <li>Data Modeling
                                    <li>Business Logic
                                    <li>API layer / Action Layer / MVC
                                    <li>User Interface
                                    <li>User Experience
                                    <li>Customer & Business requirements gathering
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Senior Lead Full Stack / Development Operations  Engineer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Corpinfo</li>
                                    <li class="employer-address">Santa Monica, CA</li>
                                    <li class="employer-date">April 2015 to June 2015</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Lead developer (Full Stack - LAMP, MEAN)</li>
                                    <li>Laravel, Lumen, Ratchet frameworks for backend/API/WebSocket communications</li>
                                    <li>Amazon AWS (Simple Queue Service (messaging), EC2 Spot Instances (large scale and distributed tasks)</li>
                                    <li>SaaS, Ecommerce, CRM, Bidding, Jobs and more</li>
                                    <li>Business & Needs Analysis</li>
                                    <li>Requirements Gathering</li>
                                    <li>Client facing Project Management and Vendor Relations</li>
                                    <li>Responsible for building a team to handle new business</li>
                                    <li>Build out development infrastructure capable of handle the needs of a typical development business. Vagrant</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Senior Lead Full Stack / Development Operations Engineer(SaaS Educational & Gaming)</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Age of Learning, Inc</li>
                                    <li class="employer-address">Glendale, CA</li>
                                    <li class="employer-date">March 2014 to October 2014</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Report directly to VP of Software to build custom, feature rich client applications</li>
                                    <li>Build  video  transcoding,  video  transfer  and  packaging,  account  management,  Customer  Relation Management  (CRM)  tools  using  a  combination  of  Bash,  PHP,  Linux  service  daemons,  FFmpeg,  MySQL, jQuery, jQueryUI, AngularJS</li>
                                    <li>Extend bespoke MVC PHP and CakePHP frameworks</li>
                                    <li>Git source control</li>
                                    <li>XML, CSV, JSON data transfer</li>
                                    <li>Linux administration</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Senior Lead Full Stack & Devops Engineer (SaaS E-Commerce)</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Buildthestore.com (Honey's Place)</li>
                                    <li class="employer-address">San Fernando, CA</li>
                                    <li class="employer-date">August 2013 to February 2014</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Work directly with CTO to architect solutions</li>
                                    <li>Development of interactive feature rich tools such as product importers, category and template management tools, customer relationship management (CRM)</li>
                                    <li>Code bespoke PHP5 OOP solutions</li>
                                    <li>Extend bespoke PHP5 MVC frameworks</li>
                                    <li>GIT source management</li>
                                    <li>CodebaseHQ issue tracking</li>
                                    <li>HTML5, CSS, JavaScript</li>
                                    <li>Advanced interactive interfaces with JQuery</li>
                                    <li>Propel ORM</li>
                                    <li>MySQL</li>
                                    <li>MemCached, Redis</li>
                                    <li>Sphinx Search</li>
                                    <li>XML, CSV, JSON</li>
                                    <li>Linux development and production environment</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Senior Technical Lead, PHP Developer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Quest Nutrition</li>
                                    <li class="employer-address">Paramount, CA</li>
                                    <li class="employer-date">2013</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Manage Magento Community Edition ecommerce platform and code base</li>
                                    <li>Work with SAP vendor to integrate Magento</li>
                                    <li>Maintain multiple company websites</li>
                                    <li>Manage contractors to ensure quality and integrity of work being done</li>
                                    <li>Work closely with internal art department and designers to obtain digital assets for company websites</li>
                                    <li>Manage all code using the git version control software</li>
                                    <li>Work directly with company directors to discuss requirements and vision</li>
                                    <li>Migrate outdated classic asp ecommerce site to the Magento platform</li>
                                    <li>Consult on best practices</li>
                                    <li>Hands on development of bespoke campaigns</li>
                                    <li>Full LAMP stack, jQuery and HTML/CSS development</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Team Lead / Lead PHP Developer / Project Manager</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Haulerdeals.com</li>
                                    <li class="employer-address">El Segundo, CA</li>
                                    <li class="employer-date">2013</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Manage Magento Enterprise ecommerce platform and code base</li>
                                    <li>Work with vendors to incorporate new technologies and services into existing code base</li>
                                    <li>Work directly with VP of technology to meet and discuss development initiatives</li>
                                    <li>Manage email campaigns with ExactTarget</li>
                                    <li>Work directly with support team to address customer issues</li>
                                    <li>Manage complete issue/bug list using CodebaseHQ and assigning issues to developers based on issue</li>
                                    <li>type and developer experience or fix the issues myself based on availability of resources</li>
                                    <li>Integrate with CDN, payment providers, cloud technologies and Amazon MWS</li>
                                    <li>Leadership and management of front-end development team</li>
                                    <li>Hands-on development and support of client-side engineering solutions which deliver outstanding and highly-performing user experience</li>
                                    <li>Current web stack web technologies; Linux, SQL, HTML5, CSS, Javascript, PHP, GIT, etc</li>
                                    <li>Working with internal and external/offshore development resources</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Contract / Freelance</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Various agencies & large brands (R/GA, Collective, SEGA, Kurt Keiger & many more)</li>
                                    <li class="employer-address">London, England</li>
                                    <li class="employer-date">August 2010 to February 2012</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Development using bespoke in-house MVC frameworks, Concrete5, PyroCMS, WordPress, Symfony, Codeigniter and Zend</li>
                                    <li>Frameworks</li>
                                    <li>Application enhancements per client request</li>
                                    <li>Website support and bug fixes</li>
                                    <li>MySQL</li>
                                    <li>JQuery</li>
                                    <li>Client facing</li>
                                    <li>SVN</li>
                                    <li>Magento Enterprise and Community Editions</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Lead PHP Developer / KE EMu Database Developer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Natural History Museum</li>
                                    <li class="employer-address">London, England</li>
                                    <li class="employer-date">May 2008 to May 2010</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Build OOP MVC PHP5 Framework to accommodate KE Software's EMu collections management database</li>
                                    <li>Build and maintain suites of application, network and performance tests implementing PHP::PDO integration with MySQL and SQL Server to store, update, retrieve and model datasets</li>
                                    <li>Use Trac to manage issues and agile user stories, SVN for version control and MediaWiki for documentation</li>
                                    <li>Build on site infrastructure and maintain existing Drupal 5/6 installations</li>
                                    <li>UNIX/Linux Administration, including Apache httpd configuration, proxy configurations, Trac and MediaWiki services, and general duties</li>
                                    <li>Meet with scientists, curators and management to gather needs and assess requirements</li>
                                    <li>Advise other PHP developers within the department on best case and with general inquiries</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Senior Developer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">D-Sire</li>
                                    <li class="employer-address">Köln, Germany</li>
                                    <li class="employer-date">June 2007 to February 2008</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>PHP4/PHP5, MySQL, XHTML, CSS and JavaScript/AJAX development</li>
                                    <li>Work closely with project managers to fulfil client requests</li>
                                    <li>Participate in needs request, brainstorming and team building sessions, taking the lead if necessary to
                                    realize correct bulletproof solutions</li>
                                    <li>Publish sites using internal content management system</li>
                                    <li>Break down of designs using appropriate tools such as Photoshop</li>
                                    <li>UNIX server administration, application deployment and network monitoring (IP Accounting)</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Senior Developer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Pop2 Internet Limited</li>
                                    <li class="employer-address">Craig y Don, North Wales, United Kingdom</li>
                                    <li class="employer-date">January 2003 to 2006</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Architect, design and develop e-commerce, database driven applications for in-house projects and clients</li>
                                    <li>Code using PHP, MySQL, XML, JavaScript, XHTML, CSS, some AJAX</li>
                                    <li>Project manage teams of developers and designers through entire project life cycle to finish projects on time and to budget</li>
                                    <li>Estimate project budgets, Review briefs and RFPs</li>
                                    <li>Meet face to-face with clients on a regular basis to discuss progress and work through changes</li>
                                    <li>Build, maintain and monitor company domain network, security policies and software installation. Windows SBS 2003, Exchange</li>
                                    <li>Administer all web servers running Windows or UNIX/Linux environment. This includes installation, setup and upgrades of all software and services. Apache(.httpd), BIND, VMWare, (Please request a full list of services and software if required)</li>
                                    <li>First line crisis management during network, service, or application outages. Nagios, CROND</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Unix Systems Analyst and Administrator</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Pennant</li>
                                    <li class="employer-address">Chicago, IL</li>
                                    <li class="employer-date">June 2001 to 2002</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Research and analyze mission critical software capacity and abilities. (Network usage, machine availability, network security/firewalls/policies/authentication and load balancing)</li>
                                    <li>Install,  configure  and  troubleshoot  key  server  software  components  to  ensure  99.99%  reliability  of information and application uptime</li>
                                    <li>Maintain recent versions of server software, patching, or manipulating source code where necessary to squash exploits and ensure stability. (Apache, Sendmail, MySQL, SSH, SSL, ISC BIND)</li>
                                    <li>Audit for security vulnerabilities</li>
                                    <li>Employ intrusion detection methods and software while personally reviewing logs to detect intrusion or mischievous behavior. Take the necessary steps to seek out intruders and their intentions. Honeynet</li>
                                    <li>Write Unix shell scripts to actively monitor, maintain and autonomously run specific tasks for a faster, more efficient network. (Bash, SNMP, MRTG)</li>
                                    <li>Review server processes and activities pinpointing the potential for high overhead or failures</li>
                                    <li>Run penetration tests on networked machines within the company to seek out possible exploitable areas of interest</li>
                                </ul>
                            </div>
                        </div>

                         <!-- -->

                        <div class="employer">
                            <h2>Freelance Senior Developer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">BuzzCo</li>
                                    <li class="employer-address">Chicago, IL</li>
                                    <li class="employer-date">July 2000 to June 2001</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Develop client applications for numerous clients as a freelancer using PHP, MySQL, XHTML, JavaScript and DHTML</li>
                                </ul>
                            </div>
                        </div>

                        <!-- -->

                        <div class="employer">
                            <h2>Senior Developer</h2>
                            <div class="toggler">
                                <ul class="employer-head">
                                    <li class="employer-name">Thirdwave LLC</li>
                                    <li class="employer-address">Chicago, IL</li>
                                    <li class="employer-date">November 1997 to July 2001</li>
                                </ul>
                                <ul class="employer-body">
                                    <li>Review Request for Proposals (RFP's) with company leads, and team. Meet with clients- traveling if necessary to discuss project requirements, project status and deadlines</li>
                                    <li>Assist upper management in high-level decision-making and the forward progression of projects in all phases of project</li>
                                    <li>Evaluate client's needs while determining best, most cost effective technologies for the job, staying within budget. Build a solid base architecture for the work. Develop visual aids for team members and clients. (E.G.: Gatorade, Cannondale, Motorola, 3Com and Caterpillar)</li>
                                    <li>Retrieve site design for image breakdown and code build-up. Code dynamic features to GUI. Work closely with Cold Fusion programmers to bring dynamic data driven content into projects. Communicate effectively with database administrators, network liaisons, team leads, vendors of payment services or other third party services and developers</li>
                                    <li>Develop applications with PHP, MySQL, JavaScript and DHTML for a wide range of uses. (Loan calculators, product  searches,  Web  Distributed  Data  eXchange  (WDDX)  dynamic  GUI  systems  and  client  side  form evaluations)</li>
                                    <li>Conduct Quality Assurance sessions with team, looking for potential problems or bugs within architecture, code or functionality. Report problems when they exist and provide legitimate solutions, documentation and innovative workarounds</li>
                                    <li>Assist in long-term maintenance of projects, network security, web services and servers on Windows and UNIX platforms</li>
                                </ul>
                            </div>

                        </div>
                    
                    </div>

                </div>

            </div>

        </div>

        <div class="topcorner dropped">

            <form id="emailform">
               
                <div class="left"><i class="fa fa-envelope fa-3"></i></div>
                <div class="left"></div>
                <div class="left">
                    <p style="width:300px;">send to a friend</p>
                    <input type="text" placeholder="first name" name="firstname" /><br />
                    <input type="text" placeholder="last name" name="lastname" /><br />
                    <input type="text" placeholder="email address" name="email" /><br />                               
                    <input type="submit" /><br />
                </div>
                <div style="clear:left"></div>
            </form>

        </div>
    
    </body>
</html>
