//--------------------------------------------------------------------[jpdr]-->
$(document).ready(function(){
	//------------------------------------------------------------------------>
    $("h2").bind('click',function(){
        $(this).next('div').slideToggle('slow');
    });

    //------------------------------------------------------------------------>
    $(".topcorner").bind('click',function(){
    	$(".forminput").slideToggle('slow');
    })

    //------------------------------------------------------------------------>
	$("#emailform").submit(function(e) {
		$.ajax({
			type:"POST",
			url:"includes/resume.php",
			data:$("#emailform").serialize(),
			success: function(data) {
				alert('Your link will arrive in your email shortly.')
			}
    	});
	    e.preventDefault(e);
	});

});
